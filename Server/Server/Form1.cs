﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        Socket socketListener;
        string txtReceive;
        string[] receiveData = new string[6];
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {

            socketListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(txtIP.Text), Convert.ToInt32(txtPORT.Text));

            //if (btnConnect.Text == "صدور مجوز ارتباط با بیمار")
            //{
            try
            {
                socketListener.Bind(ip);
                socketListener.Listen(10);
                socketListener = socketListener.Accept();

                btnConnect.Enabled = false;
                btnReceiveData.Enabled = true;
                btnSendData.Enabled = true;
                lblStatus.Text = "اطلاعات دستگاه متصل شده: " + socketListener.RemoteEndPoint.ToString();
                //btnConnect.Text = "قطع ارتباط";
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //}
            //else if (btnConnect.Text == "قطع ارتباط")
            //{
            //try
            //{
            //    socketListener.Disconnect(false);

            //    btnReceiveData.Enabled = false;
            //    btnSendData.Enabled = false;
            //    lblStatus.Text = "اطلاعات دستگاه متصل شده:  هیچ اتصالی موجود نیست";
            //    btnConnect.Text = "صدور مجوز ارتباط با بیمار";
            //}
            //catch (SocketException ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //}
        }

        private void btnReceiveData_Click(object sender, EventArgs e)
        {
            byte[] buffer = new byte[1000];

            try
            {
                socketListener.Receive(buffer);

                txtReceive = System.Text.UTF8Encoding.UTF8.GetString(buffer);
                receiveData = txtReceive.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                txtName.Text = receiveData[0];
                txtFileNumber.Text = receiveData[1];
                cbDisease.Text = receiveData[3];

                if (receiveData[2] == "مرد")
                {
                    rbMale.Checked = true;
                    rbFemale.Checked = false;
                }
                else if (receiveData[2] == "زن")
                {
                    rbMale.Checked = false;
                    rbFemale.Checked = true;
                }

                if (receiveData[4] == "1")
                    chbInsurance.Checked = true;
                else if (receiveData[4] == "0")
                    chbInsurance.Checked = false;

            }
            catch (SocketException)
            {
                MessageBox.Show("ارتباط با بیمار قطع شده است");
            }

        }

        private void btnSendData_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(txtPrescription.Text);
                socketListener.Send(buffer);
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
