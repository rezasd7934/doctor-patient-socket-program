﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtFileNumber = new System.Windows.Forms.TextBox();
            this.gbGender = new System.Windows.Forms.GroupBox();
            this.rbFemale = new System.Windows.Forms.RadioButton();
            this.rbMale = new System.Windows.Forms.RadioButton();
            this.chbInsurance = new System.Windows.Forms.CheckBox();
            this.cbDisease = new System.Windows.Forms.ComboBox();
            this.txtPrescription = new System.Windows.Forms.RichTextBox();
            this.gbDisease = new System.Windows.Forms.GroupBox();
            this.lblOtherDiseases = new System.Windows.Forms.Label();
            this.txtOtherDisease = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSendData = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPORT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRecieveData = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gbGender.SuspendLayout();
            this.gbDisease.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(24, 13);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(135, 21);
            this.txtName.TabIndex = 0;
            this.txtName.Text = "رضا سلیمانی";
            // 
            // txtFileNumber
            // 
            this.txtFileNumber.Location = new System.Drawing.Point(24, 65);
            this.txtFileNumber.Name = "txtFileNumber";
            this.txtFileNumber.Size = new System.Drawing.Size(135, 21);
            this.txtFileNumber.TabIndex = 1;
            this.txtFileNumber.Text = "1234";
            // 
            // gbGender
            // 
            this.gbGender.Controls.Add(this.rbFemale);
            this.gbGender.Controls.Add(this.rbMale);
            this.gbGender.Location = new System.Drawing.Point(47, 36);
            this.gbGender.Name = "gbGender";
            this.gbGender.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.gbGender.Size = new System.Drawing.Size(209, 119);
            this.gbGender.TabIndex = 2;
            this.gbGender.TabStop = false;
            this.gbGender.Text = "جنسیت";
            // 
            // rbFemale
            // 
            this.rbFemale.AutoSize = true;
            this.rbFemale.Location = new System.Drawing.Point(148, 67);
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.Size = new System.Drawing.Size(37, 19);
            this.rbFemale.TabIndex = 1;
            this.rbFemale.TabStop = true;
            this.rbFemale.Text = "زن";
            this.rbFemale.UseVisualStyleBackColor = true;
            // 
            // rbMale
            // 
            this.rbMale.AutoSize = true;
            this.rbMale.Location = new System.Drawing.Point(148, 28);
            this.rbMale.Name = "rbMale";
            this.rbMale.Size = new System.Drawing.Size(40, 19);
            this.rbMale.TabIndex = 0;
            this.rbMale.TabStop = true;
            this.rbMale.Text = "مرد";
            this.rbMale.UseVisualStyleBackColor = true;
            // 
            // chbInsurance
            // 
            this.chbInsurance.AutoSize = true;
            this.chbInsurance.Location = new System.Drawing.Point(10, 25);
            this.chbInsurance.Name = "chbInsurance";
            this.chbInsurance.Size = new System.Drawing.Size(42, 19);
            this.chbInsurance.TabIndex = 3;
            this.chbInsurance.Text = "بیمه";
            this.chbInsurance.UseVisualStyleBackColor = true;
            // 
            // cbDisease
            // 
            this.cbDisease.FormattingEnabled = true;
            this.cbDisease.Location = new System.Drawing.Point(208, 43);
            this.cbDisease.Name = "cbDisease";
            this.cbDisease.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbDisease.Size = new System.Drawing.Size(140, 23);
            this.cbDisease.TabIndex = 4;
            this.cbDisease.Text = "انتخاب کنید";
            this.cbDisease.SelectedIndexChanged += new System.EventHandler(this.cbDisease_SelectedIndexChanged);
            // 
            // txtPrescription
            // 
            this.txtPrescription.Enabled = false;
            this.txtPrescription.Location = new System.Drawing.Point(108, 34);
            this.txtPrescription.Name = "txtPrescription";
            this.txtPrescription.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPrescription.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.txtPrescription.Size = new System.Drawing.Size(392, 123);
            this.txtPrescription.TabIndex = 5;
            this.txtPrescription.Text = "";
            // 
            // gbDisease
            // 
            this.gbDisease.Controls.Add(this.lblOtherDiseases);
            this.gbDisease.Controls.Add(this.txtOtherDisease);
            this.gbDisease.Controls.Add(this.cbDisease);
            this.gbDisease.Location = new System.Drawing.Point(159, 189);
            this.gbDisease.Name = "gbDisease";
            this.gbDisease.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.gbDisease.Size = new System.Drawing.Size(390, 96);
            this.gbDisease.TabIndex = 6;
            this.gbDisease.TabStop = false;
            this.gbDisease.Text = "نوع بیماری";
            // 
            // lblOtherDiseases
            // 
            this.lblOtherDiseases.AutoSize = true;
            this.lblOtherDiseases.Location = new System.Drawing.Point(61, 25);
            this.lblOtherDiseases.Name = "lblOtherDiseases";
            this.lblOtherDiseases.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblOtherDiseases.Size = new System.Drawing.Size(67, 15);
            this.lblOtherDiseases.TabIndex = 10;
            this.lblOtherDiseases.Text = "سایر بیماری ها";
            this.lblOtherDiseases.Visible = false;
            // 
            // txtOtherDisease
            // 
            this.txtOtherDisease.Location = new System.Drawing.Point(37, 44);
            this.txtOtherDisease.Name = "txtOtherDisease";
            this.txtOtherDisease.Size = new System.Drawing.Size(135, 21);
            this.txtOtherDisease.TabIndex = 7;
            this.txtOtherDisease.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 16);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(99, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "نام و نام خانوادگی بیمار";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(167, 70);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "شماره پرونده";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.btnSendData);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtPORT);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtIP);
            this.groupBox1.Location = new System.Drawing.Point(47, 291);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(502, 148);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "اطلاعات سیستم";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(325, 25);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(125, 49);
            this.btnConnect.TabIndex = 15;
            this.btnConnect.Text = "برقراری ارتباط با پزشک";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnSendData
            // 
            this.btnSendData.Enabled = false;
            this.btnSendData.Location = new System.Drawing.Point(149, 95);
            this.btnSendData.Name = "btnSendData";
            this.btnSendData.Size = new System.Drawing.Size(159, 34);
            this.btnSendData.TabIndex = 14;
            this.btnSendData.Text = "ارسال اطلاعات به پزشک";
            this.btnSendData.UseVisualStyleBackColor = true;
            this.btnSendData.Click += new System.EventHandler(this.btnSendData_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(271, 42);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(30, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "پورت";
            // 
            // txtPORT
            // 
            this.txtPORT.Location = new System.Drawing.Point(196, 39);
            this.txtPORT.Name = "txtPORT";
            this.txtPORT.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPORT.Size = new System.Drawing.Size(67, 21);
            this.txtPORT.TabIndex = 13;
            this.txtPORT.Text = "2000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(149, 42);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(18, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "IP";
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(7, 39);
            this.txtIP.Name = "txtIP";
            this.txtIP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtIP.Size = new System.Drawing.Size(135, 21);
            this.txtIP.TabIndex = 11;
            this.txtIP.Text = "127.0.0.1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRecieveData);
            this.groupBox2.Controls.Add(this.txtPrescription);
            this.groupBox2.Location = new System.Drawing.Point(47, 446);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(502, 159);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "تجویز پزشک";
            // 
            // btnRecieveData
            // 
            this.btnRecieveData.Enabled = false;
            this.btnRecieveData.Location = new System.Drawing.Point(10, 59);
            this.btnRecieveData.Name = "btnRecieveData";
            this.btnRecieveData.Size = new System.Drawing.Size(94, 74);
            this.btnRecieveData.TabIndex = 15;
            this.btnRecieveData.Text = "دریافت تجویز از سمت پزشک";
            this.btnRecieveData.UseVisualStyleBackColor = true;
            this.btnRecieveData.Click += new System.EventHandler(this.btnRecieveData_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chbInsurance);
            this.groupBox3.Location = new System.Drawing.Point(47, 189);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(105, 66);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtFileNumber);
            this.groupBox4.Controls.Add(this.txtName);
            this.groupBox4.Location = new System.Drawing.Point(264, 36);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(285, 119);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(591, 624);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbDisease);
            this.Controls.Add(this.gbGender);
            this.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم اطلاعات بیمار";
            this.gbGender.ResumeLayout(false);
            this.gbGender.PerformLayout();
            this.gbDisease.ResumeLayout(false);
            this.gbDisease.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtFileNumber;
        private System.Windows.Forms.GroupBox gbGender;
        private System.Windows.Forms.RadioButton rbMale;
        private System.Windows.Forms.RadioButton rbFemale;
        private System.Windows.Forms.CheckBox chbInsurance;
        private System.Windows.Forms.ComboBox cbDisease;
        private System.Windows.Forms.RichTextBox txtPrescription;
        private System.Windows.Forms.GroupBox gbDisease;
        private System.Windows.Forms.TextBox txtOtherDisease;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPORT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Button btnSendData;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnRecieveData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblOtherDiseases;
        private System.Windows.Forms.Button btnConnect;
    }
}

