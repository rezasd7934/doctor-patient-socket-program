﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        Socket client;
        string name, fileNumber, gender, diseaseType, insurance;
        string data;
        string[] diseases =
        {
            "کرونا","فشار خون","سرطان","ام اس","دیابت","سایر بیماری ها"
        };



        public Form1()
        {
            InitializeComponent();

            cbDisease.Items.AddRange(diseases);
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            //if (btnConnect.Text == "برقراری ارتباط با پزشک")
            //{
            //Connect To Server
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                client.Connect(IPAddress.Parse(txtIP.Text), Convert.ToInt32(txtPORT.Text));

                //btnConnect.Text = "قطع ارتباط";
                btnConnect.Enabled = false;
                btnSendData.Enabled = true;
                btnRecieveData.Enabled = true;
            }
            catch (SocketException ex)
            {
                MessageBox.Show("! پزشک مجوز ارتباطی صادر نکرده است");
            }
        }
        //else if (btnConnect.Text == "قطع ارتباط")
        //{
        //    try
        //    {
        //        //client.Close();
        //        btnConnect.Text = "برقراری ارتباط با پزشک";
        //        btnSendData.Enabled = false;
        //        btnRecieveData.Enabled = false;
        //    }
        //    catch (SocketException ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}
        //}

        private void btnSendData_Click(object sender, EventArgs e)
        {
            name = txtName.Text;
            fileNumber = txtFileNumber.Text;

            if (rbMale.Checked)
                gender = "مرد";
            else if (rbFemale.Checked)
                gender = "زن";

            if (chbInsurance.Checked)
                insurance = "1";
            else if (!chbInsurance.Checked)
                insurance = "0";

            if (txtOtherDisease.Visible)
                diseaseType = txtOtherDisease.Text;

            data = "";
            data = name + "\r\n" + fileNumber + "\r\n" + gender + "\r\n" + diseaseType + "\r\n" + insurance + "\r\n";

            try
            {
                byte[] buffer = new byte[1500];
                buffer = System.Text.UTF8Encoding.UTF8.GetBytes(data);
                client.Send(buffer);
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void btnRecieveData_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] buffer = new byte[1000];
                client.Receive(buffer);
                txtPrescription.Text = System.Text.UTF8Encoding.UTF8.GetString(buffer);
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void cbDisease_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDisease.SelectedIndex < 5)
            {
                txtOtherDisease.Visible = false;
                lblOtherDiseases.Visible = false;
                diseaseType = cbDisease.Text;
            }
            else
            {
                txtOtherDisease.Visible = true;
                lblOtherDiseases.Visible = true;
            }
        }
    }
}
